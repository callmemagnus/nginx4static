FROM nginx:alpine

RUN apk update && apk add --no-cache inotify-tools

COPY conf/nginx.conf /etc/nginx/nginx.conf
COPY conf/default.conf /etc/nginx/conf.d/
COPY conf/redirect-map.conf /etc/nginx/

COPY bin/start.sh /start.sh
RUN chmod +x /start.sh

CMD "/start.sh"