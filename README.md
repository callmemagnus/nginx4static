# nginx4static

A simple-stupid image to expose a static website.

It does not handle TLS, use a proxy.

Based on `nginx:alpine`, differences:

- 404 customized page
- 50x customized page
- support for configuraing redirects based on a file named `_redirects`.
- static assets expire after 365 days

## 404

If the file 404.html is found at root level of the public directory, it will used as response to file not found.

## 50x

If the file 50x.html is found at root level of the public directory, it will used as response to server errors.

But that shouldn't be necessary ;-)

## Automatic redirections

If a file named `_redirects` is present at the root of the public directory, it will be loaded as a configuration for redirections.

The format is the following:

```
/url/to/redirect/1 https://target/of/the/redirection;
/url/to/redirect/2 https://target/of/the/redirection;
```

The file `_redirects` is hidden from "outside".

You can customize the returned file, add a 302.html file at the root of the public directory. This enables for example the use of javascript tracking snippets.

To obtain the target url in the 302.html file, use the following server side include:

```
<!--# echo var="redirect_uri" default="whatever" -->
```

## Specific `location` configuration

You can add `nginx` rules in the context of the `location /` configuration by adding a file named: `_location.part.conf`.

```
add_header X-custom-header "bob was here"
add_header Content-Security-Policy   "default-src 'none'";
```

The main use-case for that is to customize the CSP to allow stuff based on the served content.

## To run it

```sh
docker run -p8080:80 -ti -v $(pwd):/usr/share/nginx/html:ro callmemagnus/nginx4static
```
