#!/bin/bash

docker build -t callmemagnus/nginx4static .

./bin/run_tests.sh

if [ $? == 0 ]; then
    echo "PERFECT!"
else 
    echo "Please investigate."
fi

docker rmi callmemagnus/nginx4static > /dev/null

