#!/bin/bash

IMAGE=callmemagnus/nginx4static

tmp=$(mktemp -d)
waitforit=$tmp/wait-for-it.sh

status=0

curl -s https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh > $waitforit

chmod +x $waitforit

set +x

function cleanup {
	docker stop tested > /dev/null
}

function test {
	echo -n "# $1... "
	docker run \
		--name tested \
		-p8080:80 \
		-d \
		--rm \
		-v "$(pwd)/mocks/$2":/usr/share/nginx/html:ro \
		$IMAGE > /dev/null

	$waitforit localhost:8080 -q -t 3 -- sleep 2 && curl -v "http://localhost:8080/$3" 2>&1 | grep "$4" > /dev/null
	if [ $? == 0 ]; then
		echo "OK"
	else
		echo "KO"
		curl -v "http://localhost:8080/$3" 2>&1
		status=1
	fi

	cleanup
}

test "200 static page"		"static_site" 	""			"static page"

# not found
test "404 OoB"				"static_site"	"whatever"	"HTTP/1.1 404"
test "404 custom template"	"404/template"	"whatever"	"404 - custom"

# redirection
test "302 from _redirects"	"30x/OoB"		"test"		'Location: .*magooweb.com'
test "302 custom template"	"30x/template"	"test"		"302 - custom"

# test partial configuration by adding header
test "location server configuration" "location.part" "" "X-callmemagnus: here I am"

exit $status

