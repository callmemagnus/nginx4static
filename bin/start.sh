#!/bin/sh

NGINX=nginx

set -m

trap stop SIGTERM
trap stop INT

content_dir=/usr/share/nginx/html
redirect_file=_redirects
location_file=_location.part.conf

stop() {
	echo "Exiting..."
	exit 0
}

reload() {
	${NGINX} -s reload
	if ${NGINX} -s reload; then
		echo "nginx reloaded."
	else
		echo "nginx failed to reload!"
	fi
}

dirty=0

copy_config() {
	action=$1
	file=$2
	
	case $action in 
		DELETE)
			echo "$file was deleted"
			dirty=1
			rm -f "/etc/nginx/$file"
			;;
		CREATE | MODIFY)
			if [ -f "${content_dir}/${file}" ]; then
				echo "$file was created or modified"
				cp "${content_dir}/${file}" "/etc/nginx/$file"
				dirty=1
			else 
				echo "$file was created or modified but I could not find the file (if at startup, you  might ignore this message.)"
			fi
			;;
	esac
}

copy_config CREATE $redirect_file
copy_config CREATE $location_file

dirty=0

echo "nginx is starting."

if ${NGINX} -t; then
	${NGINX} -g "daemon off;" &
	if [ $? != 0 ]; then
		echo "nginx failed to start"
		exit 1
	fi
fi

# this is needed to let nginx start without the _redirects that might be broken
# sleep 1

# echo "starting _redirects loading loop."
copy_config CREATE $redirect_file
copy_config CREATE $location_file

running=0

inotifywait \
	-e modify,move,create,delete \
	-mr \
	$content_dir | while read -r dir event file; do
	
	if [ $running = 0 ]; then
		running=1
		copy_config "$event" "$file"
		copy_config "$event" "$file"

		if [ $dirty = 1 ]; then
			reload
			dirty=0
		fi
		running=0		
	fi 
done